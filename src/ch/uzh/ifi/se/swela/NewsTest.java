package ch.uzh.ifi.se.swela;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import ch.uzh.ifi.se.swela.client.model.Ballot;
import ch.uzh.ifi.se.swela.client.model.News;

@RunWith(Parameterized.class)
public class NewsTest {

	@Parameters
	public static Collection<Object[]> invalidData() {
		return Arrays.asList(new Object[][] {
				{ "", IllegalArgumentException.class },
				{ "Das ist ein Test Text", null },
		});
	}
	
	@Rule
	public ExpectedException exception = ExpectedException.none();
	@Parameter(value = 0)
	public String textInput;
	@Parameter(value = 1)
	public Class expected;
	
	News news;
	
	@Before
	public void init() {
		news = new News();
	}
	
	@Test
	public void newsTest() {
		if (expected != null)
			// tell to expect an exception!
			exception.expect(expected);
		news.setMessage(textInput);
	}

}
