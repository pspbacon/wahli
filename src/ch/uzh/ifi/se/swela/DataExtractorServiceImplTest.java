package ch.uzh.ifi.se.swela;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import ch.uzh.ifi.se.swela.server.DataExtractorServiceImpl;

public class DataExtractorServiceImplTest {

	@Test
	public void testExtract() {
		DataExtractorServiceImpl extractor = new DataExtractorServiceImpl();
		
		int stimmen[] = {};
		try {
			stimmen = extractor.extract("/war/tables/Ferien_K.csv");
		} catch (IOException e) {			
			e.printStackTrace();
			fail(e.getMessage());
		}
		
		Assert.assertNotNull(stimmen);
		// �berpr�fe ob der erwartete Wert dem aktuellen Werte entspricht. 
		Assert.assertEquals(52, stimmen.length);
		
	}

}
