package ch.uzh.ifi.se.swela.shared.model.geo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.google.appengine.api.datastore.Key;

/**
 * This is for example a canton or a city or switzerland as whole.
 * 
 * @author Brayan
 * 
 */
@Entity
public class PoliticalRegion {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Key key;
	private PoliticalGeoType polGeoType;
	private String geoName;

	public PoliticalGeoType getPolRegionType() {
		return polGeoType;
	}

	public void setPolRegionType(PoliticalGeoType polGeoType) {
		this.polGeoType = polGeoType;
	}

	public String getGeoName() {
		return geoName;
	}

	public void setGeoName(String geoName) {
		this.geoName = geoName;
	}

}
