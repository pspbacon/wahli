package ch.uzh.ifi.se.swela.shared.model.polling;

import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import ch.uzh.ifi.se.swela.shared.model.geo.PoliticalRegion;

import com.google.appengine.api.datastore.Key;

/**
 * A Poll, a Ballot.
 * 
 * @author Brayan
 * 
 */
@Entity
public class Ballot {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Key key;
	private Date DateOfPoll;
	/** Indicate if poll is closed by poll officials */
	private boolean isClosed = false;
	
	private PoliticalRegion politicalRegion;

	public Date getDateOfPoll() {
		return DateOfPoll;
	}

	public void setDateOfPoll(Date dateOfPoll) {
		DateOfPoll = dateOfPoll;
	}

	public PoliticalRegion getPoliticalRegion() {
		return politicalRegion;
	}

	public void setPoliticalRegion(PoliticalRegion politicalRegion) {
		this.politicalRegion = politicalRegion;
	}

	/**
	 * Indicates if poll has been closed by officials.
	 * @return Return true if closed.
	 */
	public boolean isClosed() {
		return isClosed;
	}

	public void setClosed(boolean isClosed) {
		this.isClosed = isClosed;
	}
	

	@Basic
	String ballotQuestion;
	// Count the votes
	private int numPositiveAnswers;
	private int numNegativeAnswers;
	private int numNeutralAnswers;

	public int getNumPositiveAnswers() {
		return numPositiveAnswers;
	}

	public void setNumPositiveAnswers(int numPositiveAnswers) {
		this.numPositiveAnswers = numPositiveAnswers;
	}

	public int getNumNegativeAnswers() {
		return numNegativeAnswers;
	}

	public void setNumNegativeAnswers(int numNegativeAnswers) {
		this.numNegativeAnswers = numNegativeAnswers;
	}

	public int getNumNeutralAnswers() {
		return numNeutralAnswers;
	}

	public void setNumNeutralAnswers(int numNeutralAnswers) {
		this.numNeutralAnswers = numNeutralAnswers;
	}
}
