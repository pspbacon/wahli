package ch.uzh.ifi.se.swela.shared.model.geo;

public enum PoliticalGeoType {
	COUNTRY, CANTON, COUNTY, CITY 
}
