package ch.uzh.ifi.se.swela.server.reportingengine.service;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Standard entity manager. Access to database objects.s
 * @author Brayan
 *
 */
public class EMFService {
	final static String PERSISTENCE_UNIT = "swela";
	
	private static final EntityManagerFactory emfInstance = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT);

	private EMFService() {
	}

	public static EntityManagerFactory get() {
		return emfInstance;
	}
}