package ch.uzh.ifi.se.swela.server;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import ch.uzh.ifi.se.swela.client.model.Ballot;
import ch.uzh.ifi.se.swela.client.model.News;
import ch.uzh.ifi.se.swela.client.service.DataExtractorService;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

public class DataExtractorServiceImpl extends RemoteServiceServlet implements
		DataExtractorService {

	private static final int NUMBER_OF_CANTONS = 26;

	/**
	 * @param address
	 *            Relative path to data file.
	 * @return Returns a single array with votes of yes and no. First set
	 *         represents yes, the second set no. A set length is determined by
	 *         the fix number of cantons.
	 */
	public int[] extract(String address) throws IOException {

		File myFile = new File(System.getProperty("user.dir") + address);
		BufferedReader br = new BufferedReader(
				new InputStreamReader(new FileInputStream(myFile), "ISO-8859-1"));
		String line = "";
		String cvsSplitBy = ",";
		int[] stimmenArray = new int[52];
		int[] jaArray = new int[26];
		int[] neinArray = new int[26];
		int i = 0;

		while ((line = br.readLine()) != null) {

			String[] output = line.split(cvsSplitBy);

			// System.out.println("Kanton: " + output[0] + " Ja: " + output[1] +
			// " Nein: " + output[2]);

			if (i > 0 && i <= NUMBER_OF_CANTONS) {
				jaArray[i - 1] = Integer
						.parseInt(output[1].replaceAll("'", ""));
				neinArray[i - 1] = Integer.parseInt(output[2].replaceAll("'",
						""));
			}

			i++;

		}

		for (int j = 0; j < (NUMBER_OF_CANTONS * 2); j++) {

			if (j <= NUMBER_OF_CANTONS - 1) {

				stimmenArray[j] = jaArray[j];
			} else {

				stimmenArray[j] = neinArray[j - NUMBER_OF_CANTONS];
			}
		}

		System.out.println("Serverside extraction done");
		br.close();
		return stimmenArray;
	}

	@Override
	public News getNews(String address) throws IOException {
		News news = new News();
		
		File myFile = new File(System.getProperty("user.dir") + address);
		BufferedReader br = new BufferedReader(
				new InputStreamReader(new FileInputStream(myFile), "ISO-8859-1"));
		int i = 0;
		String line = "";
		String[][] container = new String[10][4];
		
		while ((line = br.readLine()) != null) {
			
			String[] output = line.split(";");
			container[i][0] = output[0];
			container[i][1] = output[1];
			container[i][2] = output[2];
			container[i][3] = output[3];
			i++;
		}
		
		news.setNewsContainer(container);
		br.close();
		return news;
	}
	
	@Override
	public Ballot getVotes(int row) throws IOException {
		Ballot ballot = new Ballot();
		int rowData = row;

		File myFile = new File(System.getProperty("user.dir") + "/tables/datasheet.csv");
		BufferedReader br = new BufferedReader(
				new InputStreamReader(new FileInputStream(myFile), "ISO-8859-1"));
		int i = 0;
		String line = "";
		
		while ((line = br.readLine()) != null) {
			String[] output = line.split(",");
			if(i == rowData) {
				ballot.setDate(output[1]);
				ballot.setDecade(output[2]);
				ballot.setTitle(output[3].replace("\"",""));
				if(output[4] == "1") ballot.setAccept(true);
				else ballot.setAccept(false);
				ballot.setYesVotes(Float.parseFloat(output[5]));
				ballot.setNoVotes(Float.parseFloat(output[6]));
				ballot.setYesProportion(Double.parseDouble(output[7]));
				ballot.setNoProportion(100.0 - Double.parseDouble(output[7]));
				ballot.setYesKanton(Double.parseDouble(output[8]));
				ballot.setNoKanton(Double.parseDouble(output[9]));
				ballot.setYear(output[10]);
				ballot.setRowData(Integer.parseInt(output[11]));
				ballot.setRowBezirk(Integer.parseInt(output[12]));
			}
			i++;
		}
		
		File myFile2 = new File(System.getProperty("user.dir") + "/votedata/" + ballot.getYear() + ".csv");
		BufferedReader br2 = new BufferedReader(
				new InputStreamReader(new FileInputStream(myFile2), "ISO-8859-1"));
		int j = 0;
		String line2 = "";
		String[] kantonName = new String[26];
		int bezirkCount = ballot.getRowBezirk();
		String[] bezirkName = new String[bezirkCount];
		int rowNumber = ballot.getRowData();
		long[] jaKanton = new long[26];
		long[] neinKanton = new long[26];
		long[] jaBezirk = new long[bezirkCount];
		long[] neinBezirk = new long[bezirkCount];
		
		while((line2 = br2.readLine()) != null) {
			String[] output = line2.split(";");
			if(j == 0) {
				for(int k = 1; k < 27; k++) {
					kantonName[k-1] = output[k]; 
				}
				ballot.setKantonName(kantonName);
				for(int l = 27; l < 27 + bezirkCount; l++) {
					bezirkName[l-27] = output[l];
				}
				ballot.setBezirkName(bezirkName);
			}
			if(j == rowNumber) {
				for(int k = 1; k < 27; k++) {
					jaKanton[k-1] = Long.parseLong(output[k]);
				}
				ballot.setYesKantonVotes(jaKanton);
				for(int l = 27; l < 27 + bezirkCount; l++) {
					jaBezirk[l-27] = Long.parseLong(output[l]);
				}
				ballot.setYesBezirkVotes(jaBezirk);
			}
			if(j == rowNumber+1) {
				for(int k = 1; k < 27; k++) {
					neinKanton[k-1] = Long.parseLong(output[k]);
				}
				ballot.setNoKantonVotes(neinKanton);
				for(int l = 27; l < 27 + bezirkCount; l++) {
					neinBezirk[l-27] = Long.parseLong(output[l]);
				}
				ballot.setNoBezirkVotes(neinBezirk);
			}
			
			j++;
		}
		
		
		return ballot;
	}
}
