package ch.uzh.ifi.se.swela;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import ch.uzh.ifi.se.swela.client.model.Ballot;

@RunWith(Parameterized.class)
public class BallotDecadeTest {

	@Parameters
	public static Collection<Object[]> invalidData() {
		return Arrays.asList(new Object[][] { { "1840 - 1849", null },
				{ "1830 - 1849", IllegalArgumentException.class },
				{ "1740 - 1849", IllegalArgumentException.class },
				{ "1949 - 1940", IllegalArgumentException.class }

		});
	}

	@Rule
	public ExpectedException exception = ExpectedException.none();
	@Parameter(value = 0)
	public String decadeInput;
	@Parameter(value = 1)
	public Class expected;

	Ballot ballot = null;

	@Before
	public void init() {
		ballot = new Ballot();
	}

	@Test
	public void testSetDecadeWithException() {
		if (expected != null)
			// tell to expect an exception!
			exception.expect(expected);
		ballot.setDecade(decadeInput);

	}
}
