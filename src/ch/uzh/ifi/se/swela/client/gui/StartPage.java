package ch.uzh.ifi.se.swela.client.gui;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class StartPage extends Composite {
	
	private VerticalPanel vPanel = new VerticalPanel();

	public StartPage() {
		initWidget(this.vPanel);
		
		Label lb1 = new Label("News");
		lb1.setStyleName("big");
		lb1.addStyleName("bold");
		vPanel.add(lb1);
		vPanel.setSpacing(5);
		
		
	}

}