package ch.uzh.ifi.se.swela.client.gui;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class PrintPage extends Composite {
	
	VerticalPanel vPanel = new VerticalPanel();
	Image img;
	Label lbl;
	
	public PrintPage(Image map1, Label text) {
		initWidget(this.vPanel);
		
		this.img = map1;
		img.addClickHandler(new ClickHandler1());
		this.lbl = text;
		vPanel.add(img);
		vPanel.add(lbl);
	}
	
	public void printContent() {
		Window.print();
	}
	
	private class ClickHandler1 implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			Window.Location.reload();
		}

	
	}

}
