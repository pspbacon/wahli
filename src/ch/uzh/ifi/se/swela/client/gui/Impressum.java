package ch.uzh.ifi.se.swela.client.gui;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class Impressum extends Composite {

	VerticalPanel vPanel = new VerticalPanel();
	VerticalPanel bonus;
	
	public Impressum() {
		initWidget(this.vPanel);
		Label lbl98 = new Label("Impressum");
		lbl98.setStyleName("bold");
		lbl98.addStyleName("big");
		vPanel.add(lbl98);
		vPanel.setSpacing(4);
		Label copyright = new Label("Copyright by The Hardcore Coders - 2013");
		Label source1 = new Label("Quellen: Bundesamt für Statistik");
		Label source2 = new Label("http://www.bfs.admin.ch/bfs/portal/de/index.html");
		source2.addClickHandler(new ClickHandler1());
		
		vPanel.add(copyright);
		vPanel.add(source1);
		vPanel.add(source2);
		this.bonus = new VerticalPanel();
	}
	
	private class ClickHandler1 implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			bonus.clear();
			Image img = new Image("/images/swiss.png");
			img.setWidth("80px");
			bonus.add(img);
			vPanel.add(bonus);
		}
	}
	
}
