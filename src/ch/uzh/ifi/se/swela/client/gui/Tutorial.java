package ch.uzh.ifi.se.swela.client.gui;

import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

public class Tutorial extends Composite {
	
	VerticalPanel vPanel = new VerticalPanel();
	
	public Tutorial() {
		initWidget(this.vPanel);
		
		Label placeholder = new Label("Under construction");
		
		vPanel.add(placeholder);
	}

}
