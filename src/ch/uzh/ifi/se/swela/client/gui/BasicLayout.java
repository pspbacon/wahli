package ch.uzh.ifi.se.swela.client.gui;

import ch.uzh.ifi.se.swela.client.model.Ballot;
import ch.uzh.ifi.se.swela.client.model.News;
import ch.uzh.ifi.se.swela.client.service.DataExtractorServiceClientImpl;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.visualization.client.AbstractDataTable;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.VisualizationUtils;
import com.google.gwt.visualization.client.visualizations.PieChart;
import com.google.gwt.visualization.client.visualizations.PieChart.Options;

public class BasicLayout extends Composite {

	private VerticalPanel vPanel = new VerticalPanel();
	private HorizontalPanel hPanel;
	private VerticalPanel content;
	private DataExtractorServiceClientImpl listener;
	private VerticalPanel vPanel2;
	private Image testImage;

	public BasicLayout(DataExtractorServiceClientImpl listener) {
		initWidget(this.vPanel);
		this.listener = listener;

		this.testImage = new Image("/images/logo.png");
		vPanel.add(testImage);
		// menubar (separate implementation)
		MenuView mv = new MenuView(this);

		// titlebar
		this.hPanel = new HorizontalPanel();
		Image logo = new Image("images/cb.png");
		logo.setHeight("190px");
		
		hPanel.add(logo);
		hPanel.setSpacing(5);

		// projection area
		this.content = new VerticalPanel();
		this.vPanel2 = new VerticalPanel();

		// add everything to the Panel, basic layout through extra Panels,
		// adding spacers for cleaner layout
		Label verticalSpacer = new Label("");
		Label horizontalSpacer = new Label("");
		vPanel.add(hPanel);
		HorizontalPanel hPanel2 = new HorizontalPanel();
		vPanel.add(verticalSpacer);
		vPanel.setCellHeight(verticalSpacer, "25");
		vPanel.add(hPanel2);
		hPanel2.add(mv);
		hPanel2.add(horizontalSpacer);
		hPanel2.setCellWidth(horizontalSpacer, "50");
		hPanel2.add(content);

		// hPanel2.setCellWidth(content,"100%");

		// Set StartPage as Default
		openStart();

	}

	public DataExtractorServiceClientImpl getListener() {
		return this.listener;
	}

	public void updateContent(News news) {
		String[][] container = news.getNewsContainer();
		
	
		for(int i = 9; i > -1; i--) {
			Label date = new Label("-- " + container[i][0]);
			Label title = new Label(container[i][2]);
			Label text = new Label(container[i][3]);
			Label author = new Label("-- " + container[i][1]);
			Label lb5 = new Label(
					"--------------------------------------------------------------");
			title.setStyleName("bold");
			date.setStyleName("italic");
			content.add(date);
			content.add(title);
			content.add(text);
			content.add(author);
			content.add(lb5);
		}
		
	}

	public void displayVotes(Ballot ballot) {
		vPanel2.clear();
		HorizontalPanel hPanel2 = new HorizontalPanel();
		final HorizontalPanel hPanel3 = new HorizontalPanel();
		
		final HorizontalPanel addData = new HorizontalPanel();
		final HorizontalPanel addPie = new HorizontalPanel();


		Label titel = new Label(ballot.getTitle());
		titel.setStyleName("bold");
		Label date = new Label("Abstimmung vom " + ballot.getDate());
		date.setStyleName("bold");
		Label accept = new Label();
		if ((ballot.getYesVotes() > ballot.getNoVotes()) && (ballot.getYesKanton() > ballot.getNoKanton())){
			accept.setText("Angenommen");
			accept.setStyleName("green");
		}
		else{
			accept.setText("Abgelehnt");
			accept.setStyleName("red");
		}
		Label votesYes = new Label("Ja-Stimmen: " + ballot.getYesVotes() + "  ");
		Label votesNo = new Label("Nein-Stimmen: " + ballot.getNoVotes());
		Label votesKantonYes = new Label("Ja (Kantone): "
				+ ballot.getYesKanton() + "  ");
		Label votesKantonNo = new Label("Nein (Kantone): "
				+ ballot.getNoKanton());

		Grid stimmen = new Grid(2, 3);
		stimmen.setText(0, 0, "Ja-Stimmen: " + ballot.getYesVotes());
		stimmen.setText(0, 2, "Nein-Stimmen: " + ballot.getNoVotes());
		stimmen.setText(1, 0, "Ja (Kantone): " + ballot.getYesKanton());
		stimmen.setText(1, 2, "Nein (Kantone): " + ballot.getNoKanton());
		
		vPanel2.add(stimmen);

		vPanel2.add(titel);
		vPanel2.add(date);
		vPanel2.add(accept);
		hPanel3.add(stimmen);
		//hPanel2.add(votesYes);
		//hPanel2.add(votesNo);
		vPanel2.add(hPanel2);
		//hPanel3.add(votesKantonYes);
		//hPanel3.add(votesKantonNo);
		vPanel2.add(hPanel3);
		vPanel2.setSpacing(2);
		vPanel2.add(addData);

		// Tabellen erstellen

		TabPanel tabPanel = new TabPanel();
		addData.add(tabPanel);
		addData.add(addPie);
		final ListBox kantonBox = new ListBox();
		final ListBox bezirkBox = new ListBox();

		tabPanel.setTitle(ballot.getTitle());
		tabPanel.add(kantonBox, "Kantone", false);
		tabPanel.add(bezirkBox, "Bezirke\n", false);

		// Tabellen bev�lkern

		final String[] kantonNamen = ballot.getKantonName();
		final long[] kantonJaStimmen = ballot.getYesKantonVotes();
		final long[] kantonNeinStimmen = ballot.getNoKantonVotes();

		final String[] bezirkNamen = ballot.getBezirkName();
		final long[] bezirkJaStimmen = ballot.getYesBezirkVotes();
		final long[] bezirkNeinStimmen = ballot.getNoBezirkVotes();

		bezirkBox.setVisibleItemCount(26);
		kantonBox.setVisibleItemCount(26);

		for (int i = 0; i < 26; i++) {
			kantonBox.addItem("[" + kantonNamen[i] + "]" + "\t" + "Ja: "
					+ String.valueOf(kantonJaStimmen[i]) + "," + "\t"
					+ "Nein: " + String.valueOf(kantonNeinStimmen[i]));

		}

		for (int i = 0; i < bezirkNamen.length; i++) {
			bezirkBox.addItem("[" + bezirkNamen[i] + "]" + "\t" + "Ja: "
					+ String.valueOf(bezirkJaStimmen[i]) + "," + "\t"
					+ "Nein: " + String.valueOf(bezirkNeinStimmen[i]));
		}

		// ChangeHandler: Aktiviert wenn ListBox eintrag gew�hlt wird

		kantonBox.addChangeHandler(new ChangeHandler() {
			public void onChange(ChangeEvent event) {

				
				Runnable onLoadCallback = new Runnable() {
					public void run() {

						int selectedIndex = kantonBox.getSelectedIndex();
						// Create PieChart
						PieChart pie = new PieChart(createTable(
								kantonJaStimmen[selectedIndex],
								kantonNeinStimmen[selectedIndex]),
								createOptions(kantonNamen[selectedIndex]));

						// Check if PieChart already there, if so replace it
						// (HIER LAYOUT POSITION VERÄNDERN)
						addPie.clear();
						addPie.add(pie);
					}
				};
				
				//Calling the Google Ajax loader to get the Visualization API
				VisualizationUtils.loadVisualizationApi(onLoadCallback,
						PieChart.PACKAGE);
			}
		});

		bezirkBox.addChangeHandler(new ChangeHandler() {
			public void onChange(ChangeEvent event) {

				Runnable onLoadCallback = new Runnable() {
					public void run() {

						int selectedIndex = bezirkBox.getSelectedIndex();

						PieChart pie = new PieChart(createTable(
								bezirkJaStimmen[selectedIndex],
								bezirkNeinStimmen[selectedIndex]),
								createOptions(bezirkNamen[selectedIndex]));

						// Check if PieChart already there, if so replace it
						// (HIER LAYOUT POSITION VERÄNDERN)
						addPie.clear();
						addPie.add(pie);
					}
				};
				
				//Calling the Google Ajax loader to get the Visualization API
				VisualizationUtils.loadVisualizationApi(onLoadCallback,
						PieChart.PACKAGE);
			}
		});
	}

	// PieChart constructor Methoden

	private Options createOptions(String title) {
		Options options = Options.create();
		options.setWidth(400);
		options.setHeight(240);
		options.set3D(true);
		options.setTitle(title);
		return options;
	}

	private AbstractDataTable createTable(long kantonJaStimmen,
			long kantonNeinStimmen) {
		DataTable data = DataTable.create();
		data.addColumn(ColumnType.STRING, "Antwort");
		data.addColumn(ColumnType.NUMBER, "Stimmen");
		data.addRows(2);
		data.setValue(0, 0, "Ja");
		data.setValue(0, 1, kantonJaStimmen);
		data.setValue(1, 0, "Nein");
		data.setValue(1, 1, kantonNeinStimmen);
		return data;
	}

	// Methods to switch between the pages
	// is applied by the menubar
	public void openStart() {
		this.content.clear();
		StartPage start = new StartPage();
		this.content.add(start);

		// add here a path to a new news-unit
		getListener().getNews("/news/news.csv");
		//getListener().getNews("/news/news3.csv");
		//getListener().getNews("/news/news2.csv");
		//getListener().getNews("/news/news1.csv");
		/*
		getListener().getNews("/news/news8.txt");
		getListener().getNews("/news/news7.txt");
		getListener().getNews("/news/news6.txt");
		getListener().getNews("/news/news5.txt");
		getListener().getNews("/news/news4.txt");
		getListener().getNews("/news/news3.txt");
		getListener().getNews("/news/news2.txt");
		getListener().getNews("/news/news1.txt");
		*/
	}

	public void openApplication() {
		this.content.clear();
		ApplicationPage application = new ApplicationPage();
		this.content.add(application);
	}

	public void openImpressum() {
		this.content.clear();
		Impressum impressum = new Impressum();
		this.content.add(impressum);
	}

	public void openTutorial() {
		this.content.clear();
		Tutorial tutorial = new Tutorial();
		this.content.add(tutorial);
	}

	public void openDataPage() {
		this.content.clear();
		DataPage dataPage = new DataPage(this);
		this.content.add(dataPage);
		this.content.add(vPanel2);
	}

}
