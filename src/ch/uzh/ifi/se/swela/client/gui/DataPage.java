package ch.uzh.ifi.se.swela.client.gui;

import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.VerticalPanel;

public class DataPage extends Composite {
	
	private VerticalPanel vPanel = new VerticalPanel();
	private VerticalPanel vPanel2;
	private BasicLayout basic;
	private ListBox listYear;
	private ListBox lbox12;
	private ListBox lbox11;
	private ListBox lbox10;
	private ListBox lbox9;
	private ListBox lbox8;
	private ListBox lbox7;
	
	public DataPage(BasicLayout basic) {
		initWidget(this.vPanel);
		this.basic = basic;
		this.vPanel2 = new VerticalPanel();
		
		Label lb11 = new Label("Daten");
		lb11.setStyleName("big");
		lb11.addStyleName("bold");
		vPanel.add(lb11);
		vPanel.setSpacing(4);
		
		Label lbl1 = new Label("Wählen Sie Jahr und Abstimmung:");
		vPanel.add(lbl1);
		HorizontalPanel hPanel = new HorizontalPanel();
		
		this.listYear = new ListBox();
		listYear.setVisibleItemCount(1);
		listYear.addItem("2007");
		listYear.addItem("2008");
		listYear.addItem("2009");
		listYear.addItem("2010");
		listYear.addItem("2011");
		listYear.addItem("2012");
		listYear.setSelectedIndex(5);
		listYear.addChangeHandler(new ChangeHandler() {
			public void onChange(ChangeEvent event) {
				vPanel2.clear();
				if(listYear.getSelectedIndex() == 0) vPanel2.add(lbox7);
				if(listYear.getSelectedIndex() == 1) vPanel2.add(lbox8);
				if(listYear.getSelectedIndex() == 2) vPanel2.add(lbox9);
				if(listYear.getSelectedIndex() == 3) vPanel2.add(lbox10);
				if(listYear.getSelectedIndex() == 4) vPanel2.add(lbox11);
				if(listYear.getSelectedIndex() == 5) vPanel2.add(lbox12);
			}
		});
		hPanel.add(listYear);
		
		vPanel.add(hPanel);
		
		this.lbox7 = new ListBox();
		lbox7.addItem("Einheitskrankenkasse", "543");
		lbox7.addItem("IV-Revision", "544");
		
		this.lbox8 = new ListBox();
		lbox8.addItem("Gegen Kampfjetlärm in Tourismusregionen", "545");
		lbox8.addItem("Unternehmenssteuerreform", "546");
		lbox8.addItem("Für demokratische Einbürgerungen", "547");
		lbox8.addItem("Volksouveränität statt Behördenpropaganda", "548");
		lbox8.addItem("Wirtschaftlichkeit der Krankenversicherung", "549");
		lbox8.addItem("Unverjährbarkeit pornografischer Straftaten an Kindern", "550");
		lbox8.addItem("Für ein flexibles AHV-Alter", "551");
		lbox8.addItem("Einschränkung des Verbandbeschwerderechts", "552");
		lbox8.addItem("Hanf-Initiative", "553");
		lbox8.addItem("Revision des Betäubungsmittelgesetz", "554");
		
		this.lbox9 = new ListBox();
		lbox9.addItem("Ausweitung des Personenfreizügigkeitabkommens", "555");
		lbox9.addItem("Zukunft mit Komplementärmedizin", "556");
		lbox9.addItem("Einführung des biometrischen Passes", "557");
		lbox9.addItem("Anhebung des Mehrwertsteuersatzes", "558");
		lbox9.addItem("Verzicht auf allgemeine Volksinitiative", "559");
		lbox9.addItem("Spezialfinanzierung Luftverkehr", "560");
		lbox9.addItem("Verbot von Kriegsmaterialexporten", "561");
		lbox9.addItem("Gegen den Bau von Minaretten", "562");
		
		this.lbox10 = new ListBox();
		lbox10.addItem("Ausschaffung von kriminellen Ausländern", "567");
		lbox10.addItem("Ausweisung von kriminellen Ausländern", "568");
		lbox10.addItem("Forschung am Menschen", "563");
		lbox10.addItem("Gegen Tierquälerei", "564");
		lbox10.addItem("Mindestumwandlungssatz", "565");
		lbox10.addItem("Regelung zum Arbeitslosengesetz", "566");
		lbox10.addItem("Stopp dem Missbrauch des Steuerwettbewerbs", "569");
		
		this.lbox11 = new ListBox();
		lbox11.setVisibleItemCount(1);
		lbox11.addItem("Schutz vor Waffengewalt", "570");
		
		this.lbox12 = new ListBox();
		lbox12.setVisibleItemCount(1);
		lbox12.addItem("Bau von Zweitwohnungen", "571");
		lbox12.addItem("Bausparen", "572");
		lbox12.addItem("6 Wochen Ferien", "573");
		lbox12.addItem("Geldspiele zugunsten gemeinütziger Zwecke", "574");
		lbox12.addItem("Buchpreisbidnung", "575");
		lbox12.addItem("Eigene vier Wände", "576");
		lbox12.addItem("Stärkung der Volksrechte", "577");
		lbox12.addItem("Managed Care", "578");
		lbox12.addItem("Jugendmusikförderung", "579");
		lbox12.addItem("Sicheres Wohnen im Alter", "580");
		lbox12.addItem("Schutz vor Passivrauchen", "581");
		lbox12.addItem("Tierseuchengesetz", "582");
		
		vPanel2.add(lbox12);
		hPanel.add(vPanel2);
		
		
		Button btn1 = new Button("wählen");
		btn1.addClickHandler(new ClickHandler1());
		hPanel.add(btn1);
		hPanel.setSpacing(3);
		
		vPanel.add(hPanel);
		
		//Button btnTest = new Button("Test");
		//vPanel.add(btnTest);
		
		//btnTest.addClickHandler(new ClickHandler() {
		//	public void onClick(ClickEvent event) {		
		//		extract();
		//	}
		//});
		
	}
	
	private ListBox getList() {
		if (listYear.getSelectedIndex() == 0) return lbox7;
		else if(listYear.getSelectedIndex() == 1) return lbox8;
		else if(listYear.getSelectedIndex() == 2) return lbox9;
		else if(listYear.getSelectedIndex() == 3) return lbox10;
		else if(listYear.getSelectedIndex() == 4) return lbox11;
		else return lbox12;
	}
	
	private class ClickHandler1 implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			//System.out.println(lbox1.getValue(lbox1.getSelectedIndex()));
			String str1 = getList().getValue(getList().getSelectedIndex());
			int i = Integer.parseInt(str1);
			basic.getListener().getVotes(i);
			//basic.getListener().getVotes(lbox1.getValue(lbox1.getSelectedIndex()));
		}
	}
	
	//public void extract() {
	//	basic.getListener().extract("/tables/Ferien_K.csv");
	//}

}

