package ch.uzh.ifi.se.swela.client.gui;


import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;


public class MenuView extends Composite {
	
	private BasicLayout layout;
	VerticalPanel vPanel = new VerticalPanel();
	
	public MenuView(BasicLayout layout) {
		this.layout = layout;
		initWidget(this.vPanel);
		
		
		// Links for the Webpage
		Label navigation = new Label("Navigation");
		navigation.setStyleName("big");
		navigation.addStyleName("bold");
		Anchor anchor1 = new Anchor("Home");
		Anchor anchor5 = new Anchor("Daten");
		Anchor anchor2 = new Anchor("Karten");
		Anchor anchor3 = new Anchor("Impressum");
		Anchor anchor4 = new Anchor("Tutorial");
		
		anchor1.addClickHandler(new ClickHandler1());
		anchor2.addClickHandler(new ClickHandler2());
		anchor3.addClickHandler(new ClickHandler3());
		anchor4.addClickHandler(new ClickHandler4());
		anchor5.addClickHandler(new ClickHandler5());
		
		vPanel.add(navigation);
		vPanel.add(anchor1);
		vPanel.add(anchor5);
		vPanel.add(anchor2);
		//vPanel.add(anchor4);
		vPanel.add(anchor3);
		vPanel.setSpacing(3);
	}
	
	// open the corresponding content of the pages
	private class ClickHandler1 implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			layout.openStart();
		}
	}
	
	private class ClickHandler2 implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			layout.openApplication();
		}
	}
	
	private class ClickHandler3 implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			layout.openImpressum();
		}
	}
	
	private class ClickHandler4 implements ClickHandler {
		@Override
		public void onClick(ClickEvent event) {
			layout.openTutorial();
		}
	}
	
	private class ClickHandler5 implements ClickHandler {

		@Override
		public void onClick(ClickEvent event) {
			layout.openDataPage();
		}
	}
}