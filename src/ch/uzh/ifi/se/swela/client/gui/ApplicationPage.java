package ch.uzh.ifi.se.swela.client.gui;


import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.dom.client.ImageElement;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Hyperlink;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;

public class ApplicationPage extends Composite {
	
	// Initialization of global variables
		VerticalPanel vPanel = new VerticalPanel();
		VerticalPanel contentChoice;
		VerticalPanel contentMap;
		HorizontalPanel hPanel;
		HorizontalPanel hPanel2;
		ListBox listYear;
		ListBox list10;
		ListBox list11;
		ListBox list12;
		RadioButton radioButton1;
		RadioButton radioButton2;
		
		//the following are only for the Comment Panel
		VerticalPanel commentPanel;
		TextArea commentText;
		Button sendText;
		Button save;
		
		//for drawing the map and sending it per mail
		Image img;
		Image sendPic;
		String imageURL;
		
		
		//Constructor with default options for choice of a vote
		public ApplicationPage() {
			initWidget(this.vPanel);
			
			// title
			Label label1 = new Label("Auswahl Abstimmungen");
			vPanel.add(label1);
			
			// choice of region type
			Label label2 = new Label("Wählen Sie einen Regionstyp:");
			vPanel.add(label2);
			radioButton1 = new RadioButton("region", "Kanton");
			radioButton1.setValue(true);
			radioButton2 = new RadioButton("region", "Bezirk");
			
			// add options to panel
			hPanel2 = new HorizontalPanel();
			hPanel2.add(radioButton1);
			hPanel2.add(radioButton2);
			hPanel2.setSpacing(5);
			vPanel.add(hPanel2);
			
			Label label4 = new Label("Wählen Sie ein Jahr:");
			vPanel.add(label4);
			this.contentChoice = new VerticalPanel();
			
			// list box with the years
			this.listYear = new ListBox();
			listYear.setVisibleItemCount(1);
			listYear.addItem("2010");
			listYear.addItem("2011");
			listYear.addItem("2012");
			listYear.setSelectedIndex(2);
			listYear.addChangeHandler(new ChangeHandler() {
				public void onChange(ChangeEvent event) {
					contentChoice.clear();
					if(listYear.getSelectedIndex() == 0) contentChoice.add(list10);
					if(listYear.getSelectedIndex() == 1) contentChoice.add(list11);
					if(listYear.getSelectedIndex() == 2) contentChoice.add(list12);
				}
			});
			vPanel.add(listYear);
			
			Label label3 = new Label("Wählen Sie eine Abstimmung:");
			vPanel.add(label3);
			
			// list box with votes of corresponding year
			this.list10 = new ListBox();
			list10.setVisibleItemCount(1);
			list10.addItem("Ausschaffung von kriminellen Ausländern", "/10Auslaenderraus.svg");
			list10.addItem("Ausweisung von kriminellen Ausländern", "/10Auslaender.svg");
			list10.addItem("Forschung am Menschen", "/10Menschenforschung.svg");
			list10.addItem("Gegen Tierquälerei", "/10Tierquaelerei.svg");
			list10.addItem("Mindestumwandlungssatz", "/10BVG.svg");
			list10.addItem("Regelung zum Arbeitslosengesetz", "/10Arbeitslos.svg");
			list10.addItem("Stopp dem Missbrauch des Steuerwettbewerbs", "/10Steuerwettbewerb.svg");
			
			this.list11 = new ListBox();
			list11.setVisibleItemCount(1);
			list11.addItem("Schutz vor Waffengewalt", "/11Waffengewalt.svg");
			
			this.list12 = new ListBox();
			list12.setVisibleItemCount(1);
			list12.addItem("6 Wochen Ferien", "/12Ferien.svg");
			list12.addItem("Bausparen", "/12Bausparen.svg");
			list12.addItem("Buchpreisbindung", "/12Buchpreisbindung.svg");
			list12.addItem("Eigene Vier Wände", "/12Vierwand.svg");
			list12.addItem("Förderung der Jugendmusik", "/12Jugendmusik.svg");
			list12.addItem("Geldspiele für gemeinnützige Zwecke", "/12Geldspiele.svg");
			list12.addItem("Managed Care", "/12Managedcare.svg");
			list12.addItem("Sicheres Wohnen im Alter", "/12Alterswohnen.svg");
			list12.addItem("Staatsverträge vors Volk", "/12Staatsvertrag.svg");
			list12.addItem("Tierseuchengesetz", "/12Tierseuche.svg");
			list12.addItem("Zweitwohnungsbau", "/12Zweitwohnung.svg");
			
			hPanel = new HorizontalPanel();
			contentChoice.add(list12);		// default
			hPanel.add(contentChoice);
			
			// the button activates the image
			Button button = new Button("anzeigen");
			button.addClickHandler(new ClickHandler12());
			
			hPanel.add(button);
			hPanel.setSpacing(5);
			
			vPanel.add(hPanel);
			
			contentMap = new VerticalPanel();
			vPanel.add(contentMap);
			vPanel.setSpacing(2);
		}
		
		// get the right list back
		private ListBox getList() {
			if(listYear.getSelectedIndex() == 0) return list10;
			else if(listYear.getSelectedIndex() == 1) return list11;
			else return list12;
		}
		// applies the suitable map to the page
		// it workes as long as the Kantonmap and the Bezirkmap have the exact same name
		// now also adds text area for the comment function - BW
		private class ClickHandler12 implements ClickHandler {
			@Override
			public void onClick(ClickEvent event) {
				contentMap.clear();
				if(radioButton1.getValue()) {
					img = new Image("/images/Kanton" + getList().getValue(getList().getSelectedIndex()));
					img.setWidth("800px");
					contentMap.add(img);
				}
				if(radioButton2.getValue()) {
					img = new Image("/images/Bezirk" + getList().getValue(getList().getSelectedIndex()));
					img.setWidth("800px");
					contentMap.add(img);
				}
				
				commentPanel = new VerticalPanel();
				commentPanel.setWidth("800px");
				Label addComment = new Label("Fügen Sie hier Ihrer Auswertung einen Kommentar hinzu, bevor Sie diese abspeichern.");
				Label addComment2 = new Label("Wichtig: Um einen Zeilenabstand einzufügen verwenden Sie das Zeichen _");
				Label addComment3 = new Label("Wichtig: Jede Zeile darf nur so lange sein wie das Textfeld, danach muss ein Zeilenabstand eingefügt werden!");
				addComment.setStyleName("whitespaces");
				contentMap.setStyleName("whitespaces");
				contentMap.add(commentPanel);
				commentPanel.add(addComment);
				commentPanel.add(addComment2);
				commentPanel.add(addComment3);
				
				commentText = new TextArea();
				commentText.setWidth("800px");
				//commentText.setStyleName("textarea");
				commentPanel.add(commentText);
				
				sendText = new Button("Absenden");
				commentPanel.add(sendText);
				sendText.addClickHandler(new ClickHandler13());
					
			}
			
		}
		
		//crate canvas and add comment to the map
		private class ClickHandler13 implements ClickHandler {
			@Override
			public void onClick(ClickEvent event) {
				commentPanel.clear();
				contentMap.clear();
				
				String theText = commentText.getText();
				int stringLength = theText.length();
			    int lineSize = 70;
				int lines = 0;
				
				
				
				/*if ((stringLength%lineSize) == 0){
						lines = stringLength/lineSize;			
				}
				else{
					lines = (stringLength/lineSize) + 1;
				}
			
				String[] textLine = new String [lines];
		
				
				for (int i = 0; i<lines; i++){
					int startPos = 0 + (lineSize*i);
					int endPos = lineSize + (lineSize * i);
					if (endPos > stringLength){
						endPos = stringLength;
					}
					textLine[i] = theText.substring(startPos, endPos);
					
					if (theText.substring(startPos, startPos+1) == " "){
						int tempEnd = textLine[i].length()-1;
						textLine[i] = textLine[i].substring(2, tempEnd);
					}
					if (endPos+1 <= stringLength){
						if ((theText.substring(endPos-1, endPos) != " ") && (theText.substring(endPos, endPos+1) != " ")){
						textLine[i] = textLine[i] + "-";
					}}
					
				}
				*/
				
				//comment this part out to test the above
				String[] textLine = theText.split("_");
				lines = textLine.length;
				//comment out until here
				
				/* I decided to rely on user input after all.
				 * the splitting by number of characters was functional, but it didn't look pretty
				 * additionally I couldn't work out some bugs that I had when I wanted to make the layout look better with it
				 * test it out and give me some feedback
				 */

				int width = 800; 
		        int height = 570 + (lines * 15);

		        Canvas canvas = Canvas.createIfSupported(); 
		        if (canvas == null) {
		            contentMap.add(new Label("Sorry, your browser doesn't support the HTML5 Canvas element"));
		            return;
		          }
		        
		        canvas.setWidth(width + "px"); 
		        canvas.setHeight(height + "px"); 
		        canvas.setCoordinateSpaceWidth(width); 
		        canvas.setCoordinateSpaceHeight(height);

		        final Context2d c = canvas.getContext2d();
		        c.setFont("normal 13px Arial");
		        //c.fillText(theText,  10,  580);
		        c.drawImage((ImageElement) img.getElement().cast(), 0, 0, 800, 566);
		        
		        for (int i = 0; i<lines; i++){
					c.fillText(textLine[i], 10, (580 + (i*15)));
				}
				
		        contentMap.add(canvas);
		        //Hyperlink link = new Hyperlink("Share this page on Twitter" , "https://twitter.com/intent/tweet?original_referer=http%3A%2F%2F1.uzh-se-swela.appspot.com/&url=http%3A%2F%2F1.uzh-se-swela.appspot.com/");
		        //contentMap.add(link);
		        
		        
		        final String imageURL = canvas.toDataUrl();
		        Image sendPic = new Image(imageURL);
		        
		        //canvas.addClickHandler(new ClickHandler() {
		        	
		        /*
		        	public void onClick(ClickEvent ev) {
		        		Window.Location.assign("https://twitter.com/intent/tweet?original_referer=http%3A%2F%2F1.uzh-se-swela.appspot.com/&url=http%3A%2F%2F1.uzh-se-swela.appspot.com/");
		        		Window.Location.assign("https://twitter.com/intent/tweet?original_referer=" + imageURL);
		        	}
		        });
		        */
			}
		}		



	}