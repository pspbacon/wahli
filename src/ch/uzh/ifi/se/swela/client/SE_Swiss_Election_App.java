package ch.uzh.ifi.se.swela.client;

import ch.uzh.ifi.se.swela.client.service.DataExtractorServiceClientImpl;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.ui.RootPanel;


/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class SE_Swiss_Election_App implements EntryPoint {
	
	public void onModuleLoad() {
		DataExtractorServiceClientImpl clientImpl = new DataExtractorServiceClientImpl(GWT.getModuleBaseURL() + "dataExtractor");
		RootPanel.get().add(clientImpl.getBasicLayout());
	}
}
