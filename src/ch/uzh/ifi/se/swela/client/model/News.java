package ch.uzh.ifi.se.swela.client.model;

import java.io.Serializable;

public class News implements Serializable {
	private String date;
	private String author;
	private String title;
	private String message = "";
	private String[][] newsContainer;
	private int lines;
	
	
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		if(message.isEmpty())
			throw new IllegalArgumentException("Eine Nachricht auf eine News darf nicht leer sein.");
		this.message += message;
	}

	public int getLines() {
		return lines;
	}

	public void setLines(int lines) {
		this.lines = lines;
	}
	public String[][] getNewsContainer() {
		return newsContainer;
	}
	public void setNewsContainer(String[][] newsContainer) {
		this.newsContainer = newsContainer;
	}
	

}
