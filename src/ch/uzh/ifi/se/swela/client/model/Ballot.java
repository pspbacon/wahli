package ch.uzh.ifi.se.swela.client.model;

import java.io.Serializable;

public class Ballot implements Serializable {
	
	private int code;
	private String date;
	private String decade;
	private String title;
	private boolean accept;
	private float yesVotes;
	private float noVotes;
	private double yesProportion;
	private double noProportion;
	private double yesKanton;
	private double noKanton;
	private String year;
	private int rowData;
	private int rowKanton;
	private int rowBezirk;
	private String[] kantonName;
	private String[] bezirkName;
	private long[] yesKantonVotes;
	private long[] noKantonVotes;
	private long[] yesBezirkVotes;
	private long[] noBezirkVotes;
	
	public int getCode() {
		return code;
	}
	public void setCode(int code) {
		this.code = code;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getDecade() {
		return decade;
	}
	// Enforcing this format: "1940 - 1949"
	public void setDecade(String decade) {
		String startDec = decade.substring(0, 4);
		String endDec = decade.substring(7,11);
		int startYear = Integer.valueOf(startDec);
		int endYear = Integer.valueOf(endDec);
		if(startYear - endYear != -9)
			throw new IllegalArgumentException("In a decade the time span must be 10 years. Starting from 0 to 9");
		this.decade = decade;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public boolean isAccept() {
		return accept;
	}
	public void setAccept(boolean accept) {
		this.accept = accept;
	}
	public float getYesVotes() {
		return yesVotes;
	}
	public void setYesVotes(float yesVotes) {
		this.yesVotes = yesVotes;
	}
	public float getNoVotes() {
		return noVotes;
	}
	public void setNoVotes(float noVotes) {
		this.noVotes = noVotes;
	}
	public double getYesProportion() {
		return yesProportion;
	}
	public void setYesProportion(double yesProportion) {
		this.yesProportion = yesProportion;
	}
	public double getNoProportion() {
		return noProportion;
	}
	public void setNoProportion(double noProportion) {
		this.noProportion = noProportion;
	}
	public double getYesKanton() {
		return yesKanton;
	}
	public void setYesKanton(double yesKanton) {
		this.yesKanton = yesKanton;
	}
	public double getNoKanton() {
		return noKanton;
	}
	public void setNoKanton(double noKanton) {
		this.noKanton = noKanton;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		if(year == null || !year.matches("(((19)|(18)|20)[0-9]{2,2})"))
			throw new IllegalArgumentException("Das Jahr muss 4 Stellen aufweisen und nummerisch sein.");
		this.year = year;
	}
	public int getRowData() {
		return rowData;
	}
	public void setRowData(int rowData) {
		this.rowData = rowData;
	}
	public int getRowKanton() {
		return rowKanton;
	}
	public void setRowKanton(int rowKanton) {
		this.rowKanton = rowKanton;
	}
	public int getRowBezirk() {
		return rowBezirk;
	}
	public void setRowBezirk(int rowBezirk) {
		this.rowBezirk = rowBezirk;
	}
	public String[] getKantonName() {
		return kantonName;
	}
	public void setKantonName(String[] kantonName) {
		this.kantonName = kantonName;
	}
	public String[] getBezirkName() {
		return bezirkName;
	}
	public void setBezirkName(String[] bezirkName) {
		this.bezirkName = bezirkName;
	}
	public long[] getYesKantonVotes() {
		return yesKantonVotes;
	}
	public void setYesKantonVotes(long[] yesKantonVotes) {
		this.yesKantonVotes = yesKantonVotes;
	}
	public long[] getNoKantonVotes() {
		return noKantonVotes;
	}
	public void setNoKantonVotes(long[] noKantonVotes) {
		this.noKantonVotes = noKantonVotes;
	}
	public long[] getYesBezirkVotes() {
		return yesBezirkVotes;
	}
	public void setYesBezirkVotes(long[] yesBezirkVotes) {
		this.yesBezirkVotes = yesBezirkVotes;
	}
	public long[] getNoBezirkVotes() {
		return noBezirkVotes;
	}
	public void setNoBezirkVotes(long[] noBezirkVotes) {
		this.noBezirkVotes = noBezirkVotes;
	}

}
