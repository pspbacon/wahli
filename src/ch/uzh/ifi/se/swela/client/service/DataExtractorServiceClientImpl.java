package ch.uzh.ifi.se.swela.client.service;

import ch.uzh.ifi.se.swela.client.gui.BasicLayout;
import ch.uzh.ifi.se.swela.client.model.Ballot;
import ch.uzh.ifi.se.swela.client.model.News;

import com.google.gwt.core.client.GWT;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;

public class DataExtractorServiceClientImpl {
	
	private DataExtractorServiceAsync service;
	private BasicLayout basic;

	public DataExtractorServiceClientImpl(String url)
	{
		this.service = GWT.create(DataExtractorService.class);
		ServiceDefTarget endpoint = (ServiceDefTarget) this.service;
		endpoint.setServiceEntryPoint(url);
		
		this.basic = new BasicLayout(this);
	}
	
	public BasicLayout getBasicLayout() {
		return this.basic;
	}
	
	public void extract(String address)
	{
		this.service.extract(address, new DefaultCallback());
	}
	
	public void getNews(String address) {
		this.service.getNews(address, new DefaultCallback());
	}
	
	public void getVotes(int i) {
		this.service.getVotes(i, new DefaultCallback());
	}
	
	private class DefaultCallback implements AsyncCallback
	{

		@Override
		public void onFailure(Throwable caught) {
		
			System.out.println("An error has occured");
			
		}

		@Override
		public void onSuccess(Object result) {
			
			// HIER UPDATE METHODE DER VISUALISIERUNG AUFRUFEN
			if(result instanceof int[]) {
				int[] stimmenArray = new int[52];
				stimmenArray = (int[])result;
				System.out.println("Recieved integer array");
			
				for (int i = 0; i < 52; i++)
				{
					System.out.println(stimmenArray[i]);
				}
			}
			
			else if(result instanceof News) {
				News news = (News)result;
				basic.updateContent(news);
			}
			
			else if(result instanceof Ballot) {
				Ballot ballot = (Ballot)result;
				basic.displayVotes(ballot);
			}
			
			
		}
		
	}
}
