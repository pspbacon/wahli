package ch.uzh.ifi.se.swela.client.service;
import java.io.IOException;

import ch.uzh.ifi.se.swela.client.model.Ballot;
import ch.uzh.ifi.se.swela.client.model.News;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

@RemoteServiceRelativePath("dataExtractor")

public interface DataExtractorService extends RemoteService {

	int[] extract(String address) throws IOException;
	News getNews(String address) throws IOException;
	Ballot getVotes(int i) throws IOException;
}
