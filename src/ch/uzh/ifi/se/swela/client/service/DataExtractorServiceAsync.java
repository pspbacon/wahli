package ch.uzh.ifi.se.swela.client.service;

import com.google.gwt.user.client.rpc.AsyncCallback;

public interface DataExtractorServiceAsync {

	void extract(String address, AsyncCallback<int[]> callback);
	void getNews(String address, AsyncCallback callback);
	void getVotes(int i, AsyncCallback callback);
}
