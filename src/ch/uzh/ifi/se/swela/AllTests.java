package ch.uzh.ifi.se.swela;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ DataExtractorServiceImplTest.class, BallotTest.class, BallotDecadeTest.class, NewsTest.class })
public class AllTests {

}
